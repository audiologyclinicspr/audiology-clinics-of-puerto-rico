Audiology Clinics of Puerto Rico was established in 1976 to provide diagnostic evaluations and rehabilitation of hearing and balance disorders. We are now the largest center in the Caribbean, providing diagnostic evaluations and rehabilitation in the southern, western and northwestern coasts of PR.

Address: 2022 Doctor Pedro A. Campos, Aguadilla 00603, Puerto Rico

Phone: 787-882-8585